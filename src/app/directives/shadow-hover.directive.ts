import { Directive, ElementRef, HostListener, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appShadow]'
})
export class ShadowHoverDirective {

  constructor(private el: ElementRef, private renderer: Renderer2) {}

  @HostListener('mouseenter') addClass() {
    this.renderer.addClass(this.el.nativeElement, 'my-shadow');
  }

  @HostListener('mouseleave') removeClass() {
    this.renderer.removeClass(this.el.nativeElement, 'my-shadow');
  }
}
