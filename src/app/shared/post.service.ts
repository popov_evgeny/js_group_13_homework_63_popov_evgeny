import { Post } from './post.model';
import { EventEmitter } from '@angular/core';

export class PostService {
  changePosts = new EventEmitter<void>()
  private posts!: Post[];

  addPosts(posts: Post[]) {
    this.posts = posts;
  }

  getPosts() {
    let array!: Post[];
    if (this.posts === undefined) {
      array = [];
    } else {
      array = this.posts.slice();
    }
    return array;
  }

  onChangePosts() {
    this.changePosts.emit();
  }
}
