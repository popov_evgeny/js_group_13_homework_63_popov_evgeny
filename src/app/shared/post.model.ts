export class Post {
  constructor(
    public id: string,
    public date: string,
    public description: string,
    public title: string,
  ) {}
}
