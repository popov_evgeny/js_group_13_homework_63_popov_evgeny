import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { ContactsComponent } from './contacts/contacts.component';
import { AddComponent } from './add/add.component';
import { ToolbarComponent } from './ui/toolbar/toolbar.component';
import { HttpClientModule } from '@angular/common/http';
import { PostComponent } from './post/post.component';
import { FormsModule } from '@angular/forms';
import { NotFoundComponent } from './not-found.component';
import { PostService } from './shared/post.service';
import { ShadowHoverDirective } from './directives/shadow-hover.directive';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AboutComponent,
    ContactsComponent,
    AddComponent,
    ToolbarComponent,
    PostComponent,
    NotFoundComponent,
    ShadowHoverDirective,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule
  ],
  providers: [PostService],
  bootstrap: [AppComponent]
})
export class AppModule { }
