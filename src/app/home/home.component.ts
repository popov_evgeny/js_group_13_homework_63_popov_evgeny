import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Post } from '../shared/post.model';
import { map } from 'rxjs/operators';
import { PostService } from '../shared/post.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  posts: Post[] | null = null;
  loading = false;
  emptyServer = true;

  constructor(
    private http: HttpClient,
    private postService: PostService,
  ) {}

  ngOnInit(): void {
    this.loading = true;
    this.getPostsServer()
    this.posts = this.postService.getPosts();
    this.postService.changePosts.subscribe(() => {
      this.getPostsServer()
      this.posts = this.postService.getPosts();
    });
  }

  getPostsServer() {
    this.http.get<{[id: string]: Post}>('https://project-f65ad-default-rtdb.firebaseio.com/posts.json').pipe(map(result =>{
      if (result === null) {
        this.emptyServer = false;
        return [];
      }
      return  Object.keys(result).map(id => {
        const postData = result[id];
        return new Post (id, postData.date, postData.description, postData.title);
      });
    }))
      .subscribe( posts =>{
        this.posts = posts;
        this.postService.addPosts(posts);
        this.loading = false;
      });
  }
}
