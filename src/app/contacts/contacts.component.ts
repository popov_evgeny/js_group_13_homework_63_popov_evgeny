import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

class Info {
  constructor(
    public contacts: string,
    public about: string,
    public street: string,
  ) {}
}

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.css']
})
export class ContactsComponent implements OnInit {
  info!: Info;

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.getInfo();
  }

  getInfo() {
    this.http.get<Info>(`https://project-f65ad-default-rtdb.firebaseio.com/Information.json`).subscribe(result => {
      this.info = result;
    });
  }
}
