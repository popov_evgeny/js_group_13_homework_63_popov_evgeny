import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Post } from '../shared/post.model';
import { PostService } from '../shared/post.service';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {
  post: Post | null = null;
  postId!: string;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private http: HttpClient,
    private postService: PostService,
    ) { }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      this.postId = params['post'];
      this.getPost(this.postId);
    });
  }

  getPost(id: string) {
    this.http.get<Post>(`https://project-f65ad-default-rtdb.firebaseio.com/posts/${id}.json`).subscribe(result => {
      this.post = result;
    });
  }

  onClickDeletePost() {
    this.http.delete(`https://project-f65ad-default-rtdb.firebaseio.com/posts/${this.postId}.json`).subscribe();
    this.postService.onChangePosts();
    void this.router.navigate(['/']);
  }

}
