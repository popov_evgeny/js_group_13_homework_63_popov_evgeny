import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Post } from '../shared/post.model';
import { PostService } from '../shared/post.service';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

  title = '';
  description = '';
  post: Post | null = null;
  postId!: string;

  constructor(
    private http: HttpClient,
    private router: Router,
    private route: ActivatedRoute,
    private postService: PostService,
  ) { }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      this.postId = params['post'];
      this.getPost(this.postId);
    });
  }

  getPost(id: string) {
    this.http.get<Post>(`https://project-f65ad-default-rtdb.firebaseio.com/posts/${id}.json`).subscribe(result => {
      this.post = result;
      if (result) {
        this.title = this.post.title
        this.description = this.post.description
      }
    });
  }

  creatPost() {
    const title = this.title;
    const description = this.description;
    const date = new Date().toDateString();
    return {title, date, description};
  }

  onClickAddPost() {
    if (!this.post) {
      let post = this.creatPost();
      this.http.post('https://project-f65ad-default-rtdb.firebaseio.com/posts.json', post).subscribe();
      this.postService.onChangePosts();
    } else {
      let post = this.creatPost();
      this.http.put<Post>(`https://project-f65ad-default-rtdb.firebaseio.com/posts/${this.postId}.json`, post).subscribe();
      this.postService.onChangePosts();
    }
    void this.router.navigate(['/']);
  }
}
